import java.util.Scanner;
public class hw07 {

	public static void main(String[] args) {
		System.out.println("Enter the text");
		String text = sampleText(); // store text in a string 
		String keyWord = null; 
		System.out.println("You entered: " + text); //call sampleText method print the text
		while(true) { // if input are valid: q c w r s 
			String choose = printMenu();  // get the choice from users 
			switch(choose) { 
				case "q":
						System.out.println("Quit");
						System.exit(0); // quit bottom
						break;
				case "c":  // if c count the number of character 
						System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(text)); // call method and get num of characters
						break;
				case "w":  // if w count the number of words
						System.out.println("Number of words: " + getNumOfWords(text)); // call method and get num of words
						break;
				case "f":
						System.out.println(findText(text, keyWord)); // call method get the number of keyword
						break;
				case "r":
						System.out.println("Edited text: " + replaceExclamation(text)); // call method get the edited text
						break;
				case "s":
						System.out.println("Edited text: " + shortenSpace(text)); // call method get the edited text
						break;
			}
		}
	}
		// sample text method
		// enter a string && store the string && output the string
		public static String sampleText() {
			Scanner input = new Scanner(System.in);
			String text = input.nextLine(); //store in a string 
			return text; //return  
		}
		
		//printMenu method && output menu && return the option by a character
		public static String printMenu() {
			System.out.println("MENU\n" +    //output menu
				"c - Number of non-whitespace characters\n" + 
				"w - Number of words\n" + 
				"f - Find text\n" + 
				"r - Replace all !'s\n" + 
				"s - Shorten spaces\n" + 
				"q - Quit\n");
			System.out.println("Choose an option: ");
			Scanner input = new Scanner(System.in); 
			String menuChoose = input.next();// get the input from user 
			return menuChoose;
		}
		
		public static int getNumOfNonWSCharacters(String text) {
			int numSpace = 0; // declare integer variable to store the number of space
			for(int i = 0; i < text.length(); i++) { // loop inside the length of the text
				if(text.charAt(i) == ' ') // if there is a space 
					numSpace ++; // space number in the text
			}
			int num = text.length() - numSpace; // the length of the text except spaces
			return num;		 	
		}
		
		public static int getNumOfWords(String text) {
			int numWords = 1;  // declare integer variable to store the number of words
			for(int i = 0; i < text.length(); i++) { // loop inside the length of the text
				if(text.charAt(i) == ' ') // count words according to the space after each word
					numWords ++;  //counter
			}
			return numWords;
		}
//		
		public static int findText(String text, String keyWord) {
			Scanner myScanner = new Scanner(System.in);
			System.out.println("Enter a word or phrase to be found");
			keyWord = myScanner.next();  // store keyword
			int count =text.split(keyWord).length-1; // count the occurrences of the keyword
			return count;
		}
		
		public static String replaceExclamation(String text) {
			return text.replace("!", "."); // replace ! by .
		}
		
		public static String shortenSpace(String text) {
			return text.replace(" +", " "); // replace more than one spaces by one space
		}
}
