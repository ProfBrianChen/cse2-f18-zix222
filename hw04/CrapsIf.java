import java.util.Scanner;
public class CrapsIf {

	public static void main(String[] args) {
    // declare variables 
		int diceOne = 0;
		int diceTwo = 0;
		String slangTerm = null;
		int score = 0;
    
    // Scanner 
		System.out.println("Choose 1: Randomly cast dice \n" + "Choose 2: Enter two dices to evaluate ");		
		Scanner myScanner = new Scanner(System.in);
		int choice = myScanner.nextInt();		
		
		// if randomly, generate two random number from 1-6
		if(choice == 1) {
			diceOne = (int)(Math.random() * 6 + 1); // generate dice one values
			diceTwo = (int)(Math.random() * 6 + 1);	// generate dice two values
			System.out.println("First dice " + diceOne +"\nSecond dice " +  diceTwo);
			score = diceOne + diceTwo; // sum of dice values 
      
		// if provided dice, scanner twice 	
		}else if(choice == 2){
			System.out.println("Please enter two integers between 1 and 6");
			Scanner diceNumber = new Scanner(System.in);
			diceOne = diceNumber.nextInt(); // enter the first value
			diceTwo = diceNumber.nextInt(); // enter the second value		
		  score = diceOne + diceTwo; // sum of dice values 
      
		// test between 1-6 
		if(diceOne < 1 || diceTwo > 6 || diceTwo < 1 || diceTwo > 6) // range of dice values 
			System.out.println("Please enter integers between 1 and 6");	 // if not in the range == reminders 
		} 
				// slang Terminology 
				if(score == 2)   // sum:2
					slangTerm = "Snake Eyes";
				if (score == 3)	 // sum:3
					slangTerm = "Ace Deuce";
				 if(score == 4) { //sum:4
						if(diceOne == diceTwo) // if equal, then Hard
							slangTerm = "Hard four";
					slangTerm = "Easy Four"; // not equal, Easy
				}
				
				if(score == 5) // sum:5
					slangTerm = "Fever Five";
				if(score == 6) { // sum:6
						if(diceOne == diceTwo) //if equal, then Hard
							slangTerm = "Hard Six";
					slangTerm = "Easy Six"; //not equal, Easy
				}
				if(score == 7) //sum:7
					slangTerm = "Seven Out";
				if(score == 8) { //sum:8
						if(diceOne == diceTwo)
							slangTerm = "Hard Eight"; //if eqaul, then Hard
					slangTerm = "Easy Eight"; //not equal, Easy
				}
				if(score == 9) //sum:9
					slangTerm = "Nine"; 
				if(score == 10) { //sum:10
						if(diceOne == diceTwo) //if equal, then Hard
							slangTerm = "Hard Ten";
					slangTerm = "Easy Ten"; //not equal, Easy
				}
				if(score == 11) //sum:11
					slangTerm = "Yo-eleven";
				if(score == 12) //sum:12
					slangTerm = "Boxcars";
		System.out.println(slangTerm); //print	
	}
}