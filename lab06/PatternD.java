import java.util.Scanner;
public class PatternD {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Enter an integer between 1 - 10");
		int number = 0;
	  	// while loop to test if it's integer
		do {
			// test if integer 
			while (!myScanner.hasNextInt()) {  //if false: not integer 
				System.out.println("Please enter an integer ");  // print out 
				myScanner.next(); //delete input scan again
			}
			System.out.println("Enter an integer between 1-10"); //if true: integer 
			number = myScanner.nextInt(); //store number
		}while(number <= 1 || number >= 10); //repeat if not in the range
		System.out.println(number);
		
		 	 
		int i, j; 
		for(i = number; i > 0; i--) {
			for(j = i; j > 0; j--) {
				System.out.print(j + " ");
			}
			System.out.println(" ");
		
		}
	}
}
