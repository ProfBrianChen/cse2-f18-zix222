import java.util.Scanner;
public class CrapsSwitch {

	public static void main(String[] args) {
		int diceOne = 0;
		int diceTwo = 0;
		String slangTerm = null;
		int score = 0;
		
		System.out.println("Choose 1  \n" + "Choose 2 ");
		Scanner myScanner = new Scanner(System.in);
		int choice = myScanner.nextInt();
		
		switch(choice) {
		case 1: diceOne = (int)(Math.random() * 6 + 1);
				diceTwo = (int)(Math.random() * 6 + 1);	
				System.out.println("First dice " + diceOne 
							+"\nSecond dice " +  diceTwo);
				score = diceOne + diceTwo;
				break;
		case 2: System.out.println("Please enter two integers between 1 and 6");
				Scanner diceNumber = new Scanner(System.in);
				diceOne = diceNumber.nextInt();
				diceTwo = diceNumber.nextInt();
				// test between 1-6 
				if(diceOne < 1 || diceTwo > 6 || diceTwo < 1 || diceTwo > 6)
					System.out.println("Please enter integers between 1 and 6");
				score = diceOne + diceTwo;
				break;
		}
		
		switch(score) {
		case 2: 
			slangTerm ="Snake Eyes";
			break;
		case 3: 
			slangTerm = "Ace Deuce";
			break;
		case 4: //
			switch(diceOne) {
				case 1: slangTerm = "Easy Four";
					break;
				case 2: slangTerm = "Hard Four";
					break;
			}
			break;
		case 5: 
			slangTerm = "Fever Five";
			break;
		case 6: //
			switch(diceOne) {
				case 1: slangTerm = "Easy Six";
						break;
				case 2: slangTerm = "Easy Six";
						break;
				case 3: slangTerm = "Hard Six";
						break;
				case 4: slangTerm = "Easy Six";
						break;
				case 5: slangTerm = "Easy Six";
						break;				
      }
			break;
		case 7:
			slangTerm = "Seven Out";
			break;
		case 8: //
			switch(diceOne) {
			case 2: slangTerm = "Easy Eight";
					break;
			case 3: slangTerm = "Easy Eight";
					break;
			case 4: slangTerm = "Hard Eight";
					break;
			case 5: slangTerm = "Easy Eight";
					break;
			case 6: slangTerm = "Easy Eight";
					break;
			}
			break;
		case 9:
			slangTerm = "Nine";
			break;
		case 10: //
			switch(diceOne) {
			case 4: slangTerm = "Easy Ten";
					break;
			case 5: slangTerm = "Hard Ten";
					break;
			case 6: slangTerm = "Easy Ten";
					break;
			}
			break;
		case 11:
			slangTerm = "Yo-eleven";
			break;
		case 12:
			slangTerm = "Boxcars";
			break;
		}
		System.out.println(slangTerm);
	}
}