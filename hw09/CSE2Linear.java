import java.util.Scanner;
import java.util.Arrays;

public class CSE2Linear {

	public static void main(String[] args) {
		int[] arr = new int[15]; // declare array 
		int i = 0; // index of the array 
		int score = 0; // store the input in the array 
		Scanner myScanner = new Scanner(System.in);

		while (i < 15) { // store 15 valid input in the array 
			System.out.println("Enter 15 ascending ints for final grades in CSE2:"); 
			if (!myScanner.hasNextInt()) { // if false: not integer
				System.out.println("Error.Please enter an integer "); // print out
				myScanner.next(); // delete input scan again
				//System.out.println(i);
				continue; // back to the scanner 
			} else { 
				score = myScanner.nextInt(); 
				if (score < 0 || score > 100) { // test the range 
					System.out.println("Error. Enter a number between 0 - 100"); // if true: integer
					//System.out.println(i);
					continue; // back to the scanner if it's not inside the range 
				} 
				if (i == 0){  // if it's the first element in the array 
					arr[0] = score; // store the input as the first array 
					i++; // to the next index 
					continue;
				}				
				else if (i > 0) { // if not the first element 
					if (score <= arr[i - 1]) { // compare with the previous element 
						System.out.println("Error. Please enter an ascending number. "); 
						//System.out.println(i);
						continue;
					}
				}
				arr[i] = score;
				//System.out.println(i);
				i++;
				}
		}
		for(i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " "); // print the array
		} 
		System.out.println("\nEnter a grade to search for: ");
		int key = myScanner.nextInt(); 
		getArr(arr, key); // call the method 
	}

	public static void getArr(int[] arr1, int key) {
		int a = 0; // the left
		int b = arr1.length - 1; // the right 
		int mid = (a + b) / 2; // the mid point value 
		int counter = 0; // counter of iteration 
		while (a <= b) { 
			counter++;
			if (arr1[mid] < key) { // if smaller than the mid point value 
				a = mid + 1; // change the size
			} else if (arr1[mid] == key) { // if equals to the mid point value 
				System.out.println(arr1[mid] + " was found in the list with " + counter + " iterations"); // found message 
				break;
			} else { // if bigger than the mid point value 
				b = mid - 1; // change the size
			}
			mid = (a + b) / 2; // reset the mid point 
		}
		if (a > b) { // if all elements are checked 
			System.out.println(key + " was not found in the list with " + counter + " iterations"); // not found message 
		}
	}
}
