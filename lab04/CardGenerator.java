import java.util.*;
public class CardGenerator{
  public static void main(String args[]){
    //Declare
    int n = (int)(Math.random() * 52 + 2);
    String suitName = null;
    String identityOfCard = null;
    int identity = 13;    
    //test System.out.println(n);
     
    // if statements to assign the suit name  
    // 1-13 diamonds  14-26 clubs 27-39 hearts 40-52 spades 
    if(n >= 1 && n <= 13)
        suitName = "Diamonds";
      else if(n >= 14 && n <= 26)
        suitName = "Clubs";
        else if(n >= 27 && n <= 39)
          suitName = "Hearts";
          else if(n >= 40 && n <= 52)
            suitName = "Spades";    
   // test  System.out.println(suitName);
    
    // switch statements assign identity 
    // smallest in the suit  ace
    // biggest in the suit king
    // normal one  sequence number 
    int a = n % 13;
    switch(a){
      case 1:  identityOfCard = "Ace";
        break;
      case 2:  identityOfCard = "2";
      break;
      case 3:  identityOfCard = "3";
      break;
      case 4:  identityOfCard = "4";
      break;
      case 5:  identityOfCard = "5";
      break;
      case 6:  identityOfCard = "6";
      break;
      case 7:  identityOfCard = "7";
      break;
      case 8:  identityOfCard = "8";
      break;
      case 9:  identityOfCard = "9";
      break; 
      case 10:  identityOfCard = "10";
      break;
      case 11:  identityOfCard = "Jack";
      break;
      case 12:  identityOfCard = "Queen";
      break;
      case 13:  identityOfCard = "King";
      break;  
    }
    //test    System.out.println((String)identityOfCard);    
    System.out.println("You picked the " + identityOfCard + " of " + suitName);

    //print out names of the card with suit and identity 
    
  }
}