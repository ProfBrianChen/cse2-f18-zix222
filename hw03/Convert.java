import java.util.Scanner; 
  public class Convert{
     public static void main(String args[]){
       
        //declare
       double rainQuantityInch;
       double rainQuantityMiles;
       
        //Scanner 
        Scanner myScanner = new Scanner(System.in);
        // Get and store values from users 
        //number of acres of land affected by hurricane precipitation
        System.out.println("Enter the affected area in acres: "); 
        double area = myScanner.nextDouble(); 
        // how many inches of rain were dropped on average
        System.out.println("Enter the rainfall in the affected area: ");  
        double rainfall = myScanner.nextDouble(); 
        
        // calculation
         // quantity of rain (cubic inch) 
        rainQuantityInch = rainfall * area; 
         // cubic inch into cubic miles
        rainQuantityMiles = rainQuantityInch * 3.93147e-15;  // *3.93147e-15 to cubic miles 
        System.out.println(rainQuantityMiles + "cubic miles");
  }
}