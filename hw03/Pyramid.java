import java.util.Scanner;
public class Pyramid{
  public static void main(String args[]){
     
    // Scanner 
    Scanner myScanner = new Scanner (System.in);
    System.out.println("Enter the square side of the pyramid (input length)>> ");  // square side of the pyramid
    double side = myScanner.nextDouble();
    
    System.out.println("Enter the height of the pyramid (input height)>> "); // height of the pyramid 
    double height = myScanner.nextDouble();
    
    //  volume formula  V = (1/3) * Base * height  
    // base = side * side 
    double volume =  side * side * height / 3;  //volume of the pyramid
    System.out.println("The volume inside the pyramid is: " + volume); //print 
    
  }
}