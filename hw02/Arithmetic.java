public class Arithmetic{
  public static void main(String args[]){
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of shirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    //Declare
    double totalCostPants;
    double totalCostShirts;
    double totalCostBelts;
    
    double salesTaxPants;
    double salesTaxShirts;
    double salesTaxBelts;
   
    double totalCostNoTax;
    double totalTax;
    double totalCost;
    
    //Calculation
    //Total cost of each kind of item 
    totalCostPants = ((double)numPants * pantsPrice) * 100.0;
    totalCostShirts = ((double)numShirts * shirtPrice) * 100.0;
    totalCostBelts = ((double)numBelts * beltCost) * 100.0;
    
    //into integer
    int totalCostPants2 = (int)totalCostPants;
    int totalCostShirts2 = (int)totalCostShirts;
    int totalCostBelts2 = (int)totalCostBelts;
    
    //remain two digits
    totalCostPants = (double)totalCostPants2/ 100.0;
    totalCostShirts = (double)totalCostShirts / 100.0;
    totalCostBelts = (double)totalCostBelts / 100.0;
     
    //Sales tax charged buying all of each kind of item
    salesTaxPants = (totalCostPants * paSalesTax) * 100.0;
    salesTaxShirts = (totalCostShirts * paSalesTax) * 100.0;
    salesTaxBelts = (totalCostBelts * paSalesTax) * 100.0;
    
    //into integer
    int salesTaxPants2 = (int)salesTaxPants;
    int salesTaxShirts2 = (int)salesTaxShirts;
    int salesTaxBelts2 = (int)salesTaxBelts;
    
    //remain two digits
    salesTaxPants = (double)salesTaxPants2 / 100.0;
    salesTaxShirts = (double)salesTaxShirts2 / 100.0;
    salesTaxBelts = (double)salesTaxBelts2 / 100.0;
        
    //Total cost of purchases before tax
    totalCostNoTax = totalCostPants + totalCostShirts + totalCostBelts;
    
    //Total cost of sales tax
    totalTax = salesTaxPants + salesTaxShirts + salesTaxBelts;
    
    //Total paid for the transaction including sales tax
    totalCost = totalCostNoTax + salesTaxPants + salesTaxShirts + salesTaxBelts;
    
    //Print
    //the cost of each of item type && total sales tax paid for each item type
    System.out.println("The pants cost $" + totalCostPants + " with sales tax fee $" + salesTaxPants);
    System.out.println("The shirts cost $" + totalCostShirts + " with sales tax fee $" + salesTaxShirts);
    System.out.println("The belts cost $" + totalCostBelts + " with sales tax fee $" + salesTaxBelts);
    		  		
    //the total cost of the purchases (before tax)
    System.out.println("\nThe total cost of the items (before tax) is $" + totalCostNoTax);
    //the total sales tax
    System.out.println("The total sales tax is $" + totalTax);
    //the total cost of the purchases(including sales tax)
    System.out.println("The total cost of the transaction (with tax) is $" + totalCost); 
    
  }
}