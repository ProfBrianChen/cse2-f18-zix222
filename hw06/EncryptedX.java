import java.util.Scanner;
public class EncryptedX {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		// input 
		System.out.println("Enter an inter between 0-100 ");
		//validate input
		int number = 0;	 //declare variable number 	
		do {
			// test if integer 
			while (!myScanner.hasNextInt()) {  //if false: not integer 
				System.out.println("Please enter an integer ");  // print out 
				myScanner.next(); //delete input scan again
			}
			System.out.println("Enter an integer between 1-100"); //if true: integer 
			number = myScanner.nextInt(); //store number
		}while(number <= 0 || number >= 100); //repeat if not in the range
		System.out.println(number);
		
		//square
		int i, j; // columns and rows
		for(i = 0; i < number; i++) {  // every column
			for(j = 0; j < number; j++) { // every row
				if(i == j || (number - 1 - i) == j) // when column == row
					System.out.print(" "); // if true, print space
				else
					System.out.print("*"); //if false, print *
			} //end j for loop
			System.out.println(" "); // change row
		}//end i for loop
	}//end main class
}
