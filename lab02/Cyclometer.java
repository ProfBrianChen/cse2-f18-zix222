
public class Cyclometer{
  public static void main (String args[]){
    // input data
    int secTrip1 = 480;  // time elapsed in seconds in the first trip
    int secTrip2 = 3220; // time elapsed in seconds in the second trip
    int countsTrip1 = 1561; //number of rotations of the front wheel in the first trip
    int countsTrip2 = 9037; // number of rotations of the front wheel in the second trip
    
    // declare variables
    double wheelDiameter=27.0;  // diameter of the wheel
  	double PI=3.14159; // value of Pi
  	int feetPerMile=5280;  // Unit Calculation feet per mile
  	int inchesPerFoot=12;   // Unit Calculation inch per foot
  	int secondsPerMinute=60;  // Unit Calculation sec to minutes
	  double distanceTrip1,distanceTrip2, totalDistance;  // declare 
    
     // print out minutes and counts
    // change unit from sec to min
     System.out.println("Trip 1 took "+
       	     ((double)secTrip1/(double)secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts."); 
	   System.out.println("Trip 2 took "+
       	     ((double)secTrip2/(double)secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");

      // calculate the distace of each trip 
      distanceTrip1 = (double)countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	    distanceTrip1/=(double)inchesPerFoot*(double)feetPerMile; // Gives distance in miles
      distanceTrip2=(double)countsTrip2*wheelDiameter*PI/(double)inchesPerFoot/(double)feetPerMile;
	    totalDistance=distanceTrip1+distanceTrip2;
    
    //Print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");
	    System.out.println("The total distance was "+totalDistance+" miles");


  }
}