import java.util.Scanner;
public class Hw05 {

	public static void main(String[] args) {		
		// variable declare
		int i = 0;  // first while loop times
		int j = 0;  // second while loop times
		int fourPair = 0;  // record the times of four-pair hands
		int threePair = 0;	// record the times of three-pair hands
		int twoPair = 0; 	// record the times of two-pair hands
		int onePair = 0;		// record the times of one-pair hands
		
		// generate five random numbers from 1- 52 		
		Scanner handsGenerate = new Scanner(System.in);		
		System.out.print("How many times do you want to generate hands? \n");
	  	// while loop to test if it's integer 
	  while(handsGenerate.hasNextInt() != true) {		
		System.out.println("Please enter an integer>> ");
		handsGenerate.next(); 
	  }
	  	int Hands_Generate = handsGenerate.nextInt();
	  	System.out.println("The number of loops: " + Hands_Generate); 
	  	
	  	int a, b, c, d, e= 0;
	  	int n1, n2, n3, n4, n5 = 0;
	  	
	  	while (i < Hands_Generate) { // range of loop times
	  		n1 = (int)(Math.random() * 52 + 2);	// card one
	  		a = n1 % 13;							// remainder of card one
	  		n2 = (int)(Math.random() * 52 + 2);	// card two
  			b = n2 % 13;							// remainder of card two
  			n3 = (int)(Math.random() * 52 + 2);	// card three
  			c = n3 % 13;							// remainder of card three
  			n4 = (int)(Math.random() * 52 + 2); // card four
  			d = n4 % 13;							// remainder of card four
  			n5 = (int)(Math.random() * 52 + 2);	// card five
  			e = n5 % 13;	 						// remainder of card five
  			
  			// test duplicated
  			if((n1 != n2) || (n1 != n3) || (n1 != n4) || (n1 != n5) || (n2 != n3) 
  					|| (n2 != n4) || (n2 != n5) || (n3 != n4) || (n3 != n5) || (n4 != n5)) {
  				 // test four pair 	 
  	  			if(((b == c) && (c == d) && (d == e)) ||
  						((a == c) && (c == d) && (d == e)) ||
  						((a == b) && (b == d) && (d == e)) ||
  						((a == b) && (b == c) && (c == e)) ||
  						((a == b) && (b == c) && (c == d))){
  					fourPair += 1;  	 // incremented by one	
  					//System.out.println(fourPair + "a");
  				}
  				// test three pair
  				else if (((a == b) && (b == c)) || ((a == b) && (b == d)) || ((a == b) && (b == e)) ||
  						((a == c) && (c == d)) || ((a == c) && (c == e)) || ((a == d) && (d == e)) ||
  						((b == c) && (c == d)) || ((b == c) && (c == e)) || ((b == d) && (d == e)) ||
  						((c == d) && (d == e))) {
  					threePair += 1; // incremented by one	
  					//System.out.println(threePair + "b");
  				}
  				//test two pair
  				else if(((a == b) && (c == d)) || ((a == b) && (c == e)) ||((a == b) && (d == e)) ||  
  					((a == c) && (b == d)) || ((a == c) && (b == e)) || ((a == c) && (d == e)) ||  
  					((a == d) && (b == c)) ||((a == d) && (b == e)) ||  ((a == d) && (c == e)) ||
  					((a == e) && (b == c)) || ((a == e) && (b == d)) ||  ((a == e) && (c == d)) ||
  					((b == c) && (d == e)) || ((b == d) && (c == e)) ||  ((b == e) && (c == d)) ){
  					twoPair += 1; // incremented by one	
  					//System.out.println(twoPair + "c");
  				}
  				// test one pair 
  				else if(a == b || a == c || a == d || a == e || 
  									b == c || b == d || b == e || 
  												c == d || c == e || 
  															d == e ) {
  					onePair += 1;  // incremented by one	 
  					//System.out.println(onePair + "d");
  				}
  	  			i++; // while loop 			
  			}else
  				i--; // end of if statements
	  	} // end of while loop
	  		
	  	int total = Hands_Generate; // total hands
		double probFour = (double)fourPair / total; // probability of four kind
		double probThree = (double)threePair / total;	// probability of three kind
		double probTwo = (double)twoPair / total; // probability of two pair
		double probOne = (double)onePair / total; // probability of one pair
		System.out.println("The probability of Four-of-a-kind: " + String.format("%.3f", probFour)); // print out
		System.out.println("The probability of Three-of-a-kind:  " +  String.format("%.3f", probThree));
		System.out.println("The probability of Two-pair:  " +  String.format("%.3f", probTwo));
		System.out.println("The probability of One-pair:  " +  String.format("%.3f", probOne));  		  	
	}
}