import java.util.Scanner;
public class PatternC {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Enter an integer between 1 - 10");
		int number = 0;
	  	// while loop to test if it's integer
		do {
			// test if integer 
			while (!myScanner.hasNextInt()) {  //if false: not integer 
				System.out.println("Please enter an integer ");  // print out 
				myScanner.next(); //delete input scan again
			}
			System.out.println("Enter an integer between 1-10"); //if true: integer 
			number = myScanner.nextInt(); //store number
		}while(number <= 1 || number >= 10); //repeat if not in the range
		System.out.println(number);
		  
		int i, j, k; 
		for(i = 1; i < number + 1; i++) {
			System.out.println(" ");
			for (k = 0; k < number - i; k++) {
				System.out.print(" "); }						
			for(j = i; j >= 1; j--) {
				System.out.print( j + "");
			}
		}
	}
}