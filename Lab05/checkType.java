import java.util.Scanner;
public class checkType{
  public static void main(String[] args){
    
	  	int Course_Number = 0;
    // enter course number 
	  Scanner courseNumber = new Scanner(System.in);
	  System.out.println("Please enter your course number>> \n");
	  	// while loop to test if it's int 
	  while(courseNumber.hasNextInt() != true) {		
		System.out.println("Please enter valid info >> ");
		courseNumber.next();
	  }
	  	Course_Number = courseNumber.nextInt();
	  	System.out.println(Course_Number);
	  
	  	String Department_Name = null;
   // enter department name 
	  	Scanner departmentName = new Scanner(System.in);
        System.out.println("Please enter your department name>> \n");
        // while loop test if it's String
        while(departmentName.hasNext() != true) {
        		System.out.println("Please enter valid info >> ");
        		departmentName.nextLine();
        } 
        Department_Name = departmentName.nextLine();
		System.out.println(Department_Name);
        
		int Meet_Times = 0;
     // number of times meets in a week 
		Scanner meetTimes = new Scanner(System.in);
        System.out.println("Please enter the number of time it meets in a week>> \n");
        // while loop test if it's int
        while(meetTimes.hasNextInt()!= true ) {
        		System.out.println("Please enter valid info >> ");
        		meetTimes.next();
        }
        		Meet_Times = meetTimes.nextInt();
        		System.out.println(Meet_Times);
       
        double Class_Start = 0;
     // the time the class starts 
        System.out.println("Please enter the time the class starts>> \n");
        Scanner classStart = new Scanner(System.in);
        // while loop to check if it's double
        while(classStart.hasNextDouble()!=true) {
        		System.out.println("Please enter valid info >> ");
        		classStart.next();
        }
        		Class_Start = classStart.nextDouble();
        		System.out.println(Class_Start);
        
        String Instructor_Name = null;
     // the instructor name 
        System.out.println("Please enter the name of your instructor>> \n");
        Scanner instructorName = new Scanner(System.in);
        //while loop to check if it's String
        while(instructorName.hasNext()!=true) {
        		System.out.println("Please enter valid info >> ");
        		instructorName.next();
        }
        		Instructor_Name = instructorName.nextLine();
        		System.out.println(Instructor_Name);
       
        int Students_Number = 0;
     //number of students 
        System.out.println("Please enter the number of students>> \n");
        Scanner studentsNumber = new Scanner(System.in);
        //while loop to test if it's int
        while(studentsNumber.hasNextInt()!=true) {
        		System.out.println("Please enter valid info >> ");
        		studentsNumber.next();	
        }
        		Students_Number = studentsNumber.nextInt();
        		System.out.println(Students_Number); 
 
  }
}