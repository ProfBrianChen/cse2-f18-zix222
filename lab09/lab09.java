import java.util.*;
public class Lab09 {

	public static void main(String[] args) {
		int[] array0 = {1,2,3,4,5,6,7,8};
		/*copy(arr);
		System.out.println();
		inverter(arr);
		System.out.println();
		inverter2(arr);*/

		int[] array1 = copy(array0);
		//System.out.println();
		int[] array2 = copy(array0);
		//System.out.println();
		inverter(array0);
		print(inverter2(array1));
		int[] array3 = inverter2(array2);
		print(array3);
		
	}
	
	public static int[] copy(int[] arr) {
		int[] arrayA = new int[arr.length];
		for(int i = 0; i < arr.length; i++) {
			arrayA[i] = arr[i];
			//System.out.print( arrayA[i] + " ");
		}
		return arrayA; 
	}
	
	public static void inverter(int[] arr) {
		int[] arrayB = new int[arr.length];
		for(int i = 0; i < arr.length; i++) {
			arrayB[i] = arr[arr.length -1 -i];
			//System.out.print(arrayB[i] + " ");
		}
		print(arrayB);
	}
	
	public static int[] inverter2(int[] arr) {
		int[] arrayC = new int[arr.length]; 
		arrayC = copy(arr);
		//System.out.println();
		//inverter(array3);
		System.out.println();
		return arrayC;
	}
	
	public static void print(int[] array) {
		int[] arrayD = new int[array.length];
		arrayD = array;
		for(int j = 0; j < arrayD.length; j++) {
			System.out.print(arrayD[j] + " ");
		}
	}
}