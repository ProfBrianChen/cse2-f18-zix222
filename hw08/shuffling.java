import java.util.Scanner;

public class Shuffling {
	public static void main(String[] args) { 
		Scanner scan = new Scanner(System.in); 
		//suits club, heart, spade or diamond 
		String[] suitNames={"C","H","S","D"};    
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
		String[] cards = new String[52]; 
		String[] hand = new String[5]; 
		int numCards = 5; 
		int again = 1; 
		int index = 51;
		for (int i=0; i<52; i++){ 
			cards[i]=rankNames[i%13]+suitNames[i/13]; 
			System.out.print(cards[i]+" "); 
		} 
		System.out.println();
		printArray(cards); 
		System.out.println("Shuffled");
		shuffle(cards); 
		printArray(cards); 
		System.out.println("Hand");
		while(again == 1){ 
			hand = getHand(cards,index,numCards); 
			printArray(hand);
			index = index - numCards;
			System.out.println("Enter a 1 if you want another hand drawn"); 
			again = scan.nextInt(); 
		}  
	}

	public static void printArray(String[] cards) {
		for(int i = 0; i < cards.length; i++) { // for loop to create a deck
			System.out.print( cards[i] + "  " );  // print out the cards in order 
		}
		System.out.println(); // change lines 
	}
	
	public static String[] shuffle(String[] cards) {
		for (int i=0; i<cards.length; i++) { // loop inside the size of the array 
			//find a random member to swap with
			int target = (int) (cards.length * Math.random() ); // take a random number to switch cards 
			//swap the values
			String temp = cards[target]; // create one place to switch two others 
			cards[target] = cards[i];
			cards[i] = temp;
		}
		return cards; // return 
	}
	
	private static String[] getHand(String[] cards, int index, int numCards) {
		// make array 
		String[] arr = new String[numCards];
		if(index < numCards) {     // if index smaller than at most five cards 
			String[] suitNames={"C","H","S","D"};   
			String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
			System.out.println("A new deck is created: ");
			for (int i=0; i<52; i++){   // create a new deck 
				cards[i]=rankNames[i%13]+suitNames[i/13]; 
				System.out.print(cards[i]+" "); 
			} 
			shuffle(cards); // shuffled the decks and print 
			System.out.println("Shuffle");
			printArray(cards);
			System.out.println();
			 index = 51;
			 System.out.println("Hand");
		}
		
		for(int i = 0; i < numCards; i++) {  // print the five cards  
			arr[i] = cards[index - i]; // store the five cards in a new array and print
		}
		return arr;
	}
}