
public class Lab12 {

	public static void main(String[] args) {
		int[] array = {9,6,7,3,2,5,1,4,8};
		int[] array2 = {7,9,6,3,2,5,1,4,8};
		int[] array3 = {9,8,7,6,5,4,3,2,1};
		int[] array4 = {1,2,3,4,5,6,7,8,9};
		// selection sort
		int num = (array.length * (array.length + 1)) / 2 - 1;
		int counter = 0;
		for(int i = 0; i < array.length - 1; i++) {
			int min = array[i];
			int min_Index = i;	
			
			for(int j = i + 1; j < array.length; j++) {
				if(min > array[j]) {
					min = array[j];
					min_Index = j;
				}
			}
			System.out.println();
			printArray(array);
			
			if(min != i) {
				array[min_Index] = array[i];
				array[i] = min;
				
			}
		}
		System.out.println();
		printArray(array);
		System.out.println("\nThe total number of operations performed: " + num );
		
		// insertion sort 
		 int i, key, j; 
		   for (i = 1; i < array3.length; i++) 
		   { 	
		       key = array3[i]; 
		       j = i-1; 
		       while (j >= 0 && array3[j] > key) 
		       { 
		           array3[j+1] = array3[j]; 
		           j = j-1; 
		           counter += 1;
		       } 
		       array3[j+1] = key;
		       System.out.println();
				printArray(array3);
		
		   }
		   System.out.println("\nThe total number of operations performed: " + counter);		   
	}	
	
	
	public static void printArray(int[] arr) {
		for(int k = 0; k < arr.length; k++) {
			System.out.print(arr[k] + " ");
		}
	}
}