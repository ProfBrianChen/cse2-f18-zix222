import java.util.Scanner;

public class RemoveElements {
	public static void main(String[] arg) {
		Scanner scan = new Scanner(System.in);
		int num[] = new int[10];
		int newArray1[];
		int newArray2[];
		int index, target;
		String answer = "";
		do {
			System.out.print("Random input 10 ints [0-9]");
			num = randomInput();
			String out = "The original array is:";
			out += listArray(num);
			System.out.println(out);

			System.out.print("Enter the index ");
			index = scan.nextInt();
			newArray1 = delete(num, index);
			String out1 = "The output array is ";
			out1 += listArray(newArray1); // return a string of the form "{2, 3, -9}"
			System.out.println(out1);

			System.out.print("Enter the target value ");
			target = scan.nextInt();
			newArray2 = remove(num, target);
			String out2 = "The output array is ";
			out2 += listArray(newArray2); // return a string of the form "{2, 3, -9}"
			System.out.println(out2);

			System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
			answer = scan.next();
		} while (answer.equals("Y") || answer.equals("y"));
	}

	public static String listArray(int num[]) {
		String out = "{";
		for (int j = 0; j < num.length; j++) {
			if (j > 0) {
				out += ", ";
			}
			out += num[j];
		}
		out += "} ";
		return out;
	}
	// random input 
	public static int[] randomInput() { 
		int[] random = new int[10];
		for (int i = 0; i < random.length; i++) { // inside the length 
			random[i] = (int) (Math.random() * 10); // generate random number 
		}
		System.out.println(); // print out the array 
		return random;
	}

	public static int[] delete(int random[], int i) {
		if (i < 0 || i > random.length) { // if the index is invalid 
			System.out.println("The index is not valid."); // error message 
			return random; // return the original array 
		} else {
			int l = 0;
			l = random.length - 1; // change the size 
			for (int j = i; j < l; j++) { // inside the array 
				random[j] = random[j + 1]; // add in the number 
			}
			System.out.println("Index " + i + " element is removed"); // print our the message 
			return random; // return array 
		}
	}
	// remove elements 
	public static int[] remove(int random[], int target) { 
		int count = 0; // declare the counter 
		for (int i = 0; i < random.length; i++) { // inside the array 
			if (random[i] == target) // if equal to the target 
				count++; // counter + 1
		}
		if (count == 0) { // if counter is zero 
			return random; // target not found 
		}
		int[] arrayR = new int[random.length - count]; // change the array size 
		int index = 0;
		for (int j = 0; j < random.length; j++) { // inside the array 
			if (random[j] != target) { // if target not found 
				arrayR[index] = random[j]; // change the index number 
				index++;
			}
		}
		System.out.println("Element " + target + " has been found."); // element found 
		return arrayR; // return 
	}
}

